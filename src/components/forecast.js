import React, { Component } from 'react';
import './forecast.css';

class Forecast extends React.Component {
    getDayOfWeek(date) {
        switch(date) {
            case 0:
                return 'Sunday';
            case 1:
                return 'Monday';
            case 2:
                return 'Tuesday';
            case 3:
                return 'Wednesday';
            case 4:
                return 'Thursday';
            case 5:
                return 'Friday';
            case 6:
            default:
                return 'Saturday';
        }
    }
    getMonth(monthCode) {
        switch(monthCode) {
            case 0:
                return 'Jan';
            case 1:
                return 'Feb';
            case 2:
                return 'Mar';
            case 3:
                return 'Apr';
            case 4:
                return 'May';
            case 5:
                return 'Jun';
            case 6:
                return 'Jul';
            case 7: 
                return 'Aug';
            case 8: 
                return 'Sept';
            case 9: 
                return 'Oct';
            case 10:
                return 'Nov';
            default:
                return 'Dec';
        }
    }
    getDayString(date) {
        let dateCode = Date.parse(date);
        let dt = new Date(Number(dateCode.toString()));
        return `${this.getDayOfWeek(dt.getDay())} ${dt.getDate()} ${this.getMonth(dt.getMonth())}`;
    }
    render() {
        if (!this.props.value) return (<div className="forecast"></div>);
        return (
            <div className="forecast col">
                <div className="col">
                    {this.props.value.forecast.forecastday.map((obj) => {
                        return (
                            <div className="list-item row">
                                <div className="col">
                                    <div><img src={obj.day.condition.icon} className="condition-icon" alt=""></img></div>
                                </div>
                                <div className="col">
                                    <div>{this.getDayString(obj.date)}</div>
                                    <div>{obj.day.condition.text}</div>
                                </div>
                                <div className="col">
                                    <div>
                                        High {obj.day.maxtemp_f}&deg;F 
                                    </div>
                                    <div>
                                        Low {obj.day.mintemp_f}&deg;F 
                                    </div>
                                </div>
                            </div>
                        );
                    })}    
                </div> 
            </div>
        );
    }
}

export default Forecast;