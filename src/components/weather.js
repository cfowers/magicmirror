import React, { Component } from 'react';
import './weather.css';

class Weather extends React.Component {
    render() {
        if (!this.props.value) return (<div className="weather"></div>);
        return (
            <div className="weather">
                <div>{this.props.value.location.name}</div>
                <div className="condition">
                    <img src={this.props.value.current.condition.icon} style={{height: '64px'}}></img>
                    <div>{this.props.value.current.condition.text} - {this.props.value.current.temp_f}&deg;f</div>
                </div>
            </div>
        );
    }
}

export default Weather;