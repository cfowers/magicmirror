import React, { Component } from 'react';

class NewsTicker extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
    }
    render() {
        if (!this.props.value.currentArticle) 
            return (<div className="news-ticker"></div>);
        return (
            <div className="news-ticker">
            <div className="article">
                <div>{this.props.value.currentArticle.title}</div>
            </div>
            </div>
        );
    }
}

export default NewsTicker;