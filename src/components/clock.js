import React, { Component } from 'react';

class Clock extends React.Component {
    render() {
        return (
            <div className="clock">
                <h1>{this.props.value.date}</h1>
                {this.props.value.time}
            </div>
        );
    }
}

export default Clock;