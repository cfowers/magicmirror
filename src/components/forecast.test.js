import React from 'react';
import ReactDOM from 'react-dom';
import Forecast from './forecast';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';
import { JSDOM } from 'jsdom';

configure({ adapter: new Adapter() });

const doc = new JSDOM('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

let props;
beforeEach(() => {
    props = {
        forecast: {
            forecastday: [
                {
                    date: '2018-12-04',
                    day: {
                        condition: {
                            text: "Partly Cloudy",
                            icon: "https://made.up.url/nologo.jpg"
                        },
                        maxtemp_f: 59.5,
                        mintemp_f: 32.1
                    }
                }
            ]
        }
    };
});

describe('Testing Forecast', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Forecast {...props} />);
    });

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Forecast />, div);
        ReactDOM.unmountComponentAtNode(div);
        expect(!!wrapper.instance()).equals(true);
    });

    describe('Get Day Of Week', () => {
        it('returns Sunday for day 0', () => {
            expect(wrapper.instance().getDayOfWeek(0)).equals('Sunday');
        });
        it('returns Monday for day 1', () => {
            expect(wrapper.instance().getDayOfWeek(1)).equals('Monday');
        });
        it('returns Tuesday for day 2', () => {
            expect(wrapper.instance().getDayOfWeek(2)).equals('Tuesday');
        });
        it('returns Wednesday for day 3', () => {
            expect(wrapper.instance().getDayOfWeek(3)).equals('Wednesday');
        });
        it('returns Thursday for day 4', () => {
            expect(wrapper.instance().getDayOfWeek(4)).equals('Thursday');
        });
        it('returns Friday for day 5', () => {
            expect(wrapper.instance().getDayOfWeek(5)).equals('Friday');
        });
        it('returns Saturday for day 6', () => {
            expect(wrapper.instance().getDayOfWeek(6)).equals('Saturday');
        });
    }); 

    describe('Get Month', () => {
        it('returns Jan for 0', () => {
            expect(wrapper.instance().getMonth(0)).equals('Jan');
        });
        it('returns Feb for 1', () => {
            expect(wrapper.instance().getMonth(1)).equals('Feb');
        });
        it('returns Mar for 2', () => {
            expect(wrapper.instance().getMonth(2)).equals('Mar');
        });
        it('returns Apr for 3', () => {
            expect(wrapper.instance().getMonth(3)).equals('Apr');
        });
        it('returns May for 4', () => {
            expect(wrapper.instance().getMonth(4)).equals('May');
        });
        it('returns Jun for 5', () => {
            expect(wrapper.instance().getMonth(5)).equals('Jun');
        });
        it('returns Jul for 6', () => {
            expect(wrapper.instance().getMonth(6)).equals('Jul');
        });
        it('returns Aug for 7', () => {
            expect(wrapper.instance().getMonth(7)).equals('Aug');
        });
        it('returns Sept for 8', () => {
            expect(wrapper.instance().getMonth(8)).equals('Sept');
        });
        it('returns Oct for 9', () => {
            expect(wrapper.instance().getMonth(9)).equals('Oct');
        });
        it('returns Nov for 10', () => {
            expect(wrapper.instance().getMonth(10)).equals('Nov');
        });
        it('returns Dec for 11', () => {
            expect(wrapper.instance().getMonth(11)).equals('Dec');
        });
    });

    describe('GetDayString', () => {
        it('should return a string from a valid date', () => {
            expect(wrapper.instance().getDayString('2018-12-01T00:00:00')).equals('Saturday 1 Dec');
        });
    })
});


