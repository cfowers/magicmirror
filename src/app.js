import React, { Component } from 'react';
import logo from './logo.svg';
import './app.css';
import Clock from './components/clock';
import Forecast from './components/forecast';
import Weather from './components/weather';
import NewsTicker from './components/news';

import * as config from './config.json'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dt: {
        date: new Date().toDateString(), 
        time: new Date().toTimeString()
      },
      forecast: null,
      news: { 
        currentIndex: 0
      }
    }
    this.updateTime();
    this.updateWeather();
    this.updateNews();
  }

  updateTime() {
    setInterval(() => {
      this.setState({
        dt: {
          date: new Date().toDateString(),
          time: new Date().toTimeString()
        }
      });
    }, 900);
  }

  updateWeather() {
    this.getWeather();
    setInterval(() => {
      this.getWeather();
    }, 3600000);
  }

  updateNews() {
    this.getNews();
    setInterval(() => {
      if (this.state.news.articles) {
        let currentState = this.state.news;
        currentState.currentIndex++;
        if (currentState.currentIndex >= currentState.articles.length) currentState.currentIndex = 0;
        currentState.currentArticle = currentState.articles[currentState.currentIndex]
        this.setState({news: currentState});
      }
    }, 5000);
  }

  async getNews() {
    const api_call = await fetch(`https://newsapi.org/v2/top-headlines?country=us&apiKey=${config.newsApiKey}`);
    let response = await api_call.json();
    response.currentIndex = 0;
    this.setState({news: response});
  }

  async getWeather() {
    const api_call = await fetch(`https://api.apixu.com/v1/forecast.json?days=5&key=${config.weatherApiKey}&q=Ooltewah`);
    let response = await api_call.json(); 
    this.setState({forecast: response});
  }

  render() {
    return (
      <div className="app">
        <div className="col">
          <div className="row">
            <div className="col flex-1">
              <img src={logo} className="app-logo" alt="logo" />
            </div>
            <div className="col flex-1">
              <Weather value={this.state.forecast}/>
              <Clock value={this.state.dt} />
            </div>
            <Forecast value={this.state.forecast}/>
          </div>
          <div className="row">
            <NewsTicker value={this.state.news} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
